# quoter
A Simple Quote Generator - try it out here: https://samuelts.com/quoter

# Current Status
Quote Machine currently makes an XHR request to https://talaikis.com/api/quotes/random/ then types out the resulting quote and author on screen for the user. Master branch utilizes a function to open twitter via javascript, FCC branch utilizes a link \<a\> element and modifies it on quote generation to open twitter.

Additionally the quote machine will, in theory, catch HTTP errors and report them to the user by writing a quote and attributing it to HTTP.

# Images
![Text](./ReadMe_Assets/QuoterText.png)

![Credits](./ReadMe_Assets/QuoterCredits.png)
