//Get Buttons for later assignment
var newQuoteBtn = document.getElementById('new-quote');
var tweetBtn = document.getElementById('tweet-quote');
var creditsBtn = document.getElementById('credits');
var themeSwitch = document.getElementById('theme-switch')
var themeToggle = document.getElementById('toggle-theme');
var moonIcon = document.getElementById('moon');
var sunIcon = document.getElementById('sun');
var overlay = document.getElementById('creditsOverlay');
var text = document.getElementById('text');
var author = document.getElementById('author');
var quoteBox = document.getElementById('quote-box');
var wpmSlider = document.getElementById('wpm-slider');
var wpmDisplay = document.getElementById('wpm-display');

var themeElements = document.querySelectorAll('.dark-element, .light-element');

var printSpd = 40; //ms per letter

//Create var/div for holding quote (keeps innerhtml on text div happy)
var q = '';
var qDiv = document.createElement('div');
qDiv.id = 'qDiv';
document.getElementById('text').appendChild(qDiv);

//Create var/div for holding author
var a = '';
var aDiv = document.createElement('div');
aDiv.id = 'aDiv';
document.getElementById('author').appendChild(aDiv);

//Use AJAX request to fetch quote and author
function getQuote() {
  newQuoteBtn.disabled = true;
  if (overlay.style.display === "block") {
    showCredits();
  }
  delQ();
  var request = new XMLHttpRequest();
  var quoteAPIURL = 'https://talaikis.com/api/quotes/random';
  request.open('GET', quoteAPIURL, true);
  request.onreadystatechange = () => {
    if (request.readyState === 4) {
      if (request.status === 200) {
        var data = JSON.parse(request.response);
        q = '"' + data.quote + '"';
        a = data.author;
        printQuote();
      } else {
        if (request.status === 404) {
          q = '"Error ' + request.status + ', resource not found."';
        } else if (request.status === 401) {
          q = '"Error ' + request.status + ', user is unauthorized to access this."';
        } else if (request.status === 403) {
          q = '"Error ' + request.status + ', server refused action."';
        } else {
          q = '"Error unknown, request was unable to complete."';
          console.log('Error returned, request status: ' + request.status);
        }
        a = 'HTTP';
        printQuote();
        newQuoteBtn.disabled = false;
      }
    }
  }
  request.send();
}

//Delete quote and author variables and clear respective divs
function delQ() {
  q = "";
  a = "";
  qDiv.innerHTML = "";
  aDiv.innerHTML = "";
}

//Print quote by character to simulate typing
function printQuote() {
  newQuoteBtn.disabled = false;
  let i = 0;
  let j = 0;
  printQ();

  function printQ() {
    document.addEventListener('click', () => {
      qDiv.innerHTML = q;
      i = q.length;
    }, false);
    if (i < q.length) {
      qDiv.innerHTML += q[i];
      i++;
      setTimeout(printQ, printSpd);
    } else if (i === q.length) {
      printA();
    }
  }
  //Print author's name in chunks by name (e.g. first, then last)
  function printA() {
    if (j === 0) {
      aDiv.innerHTML = "- ";
    }
    aW = a.split(/(?=\s)/g);
    if (j < aW.length) {
      aDiv.innerHTML += aW[j];
      j++;
      setTimeout(printA, 500);
    }
  }

}

//convert words per min to ms per letter
function changeSpeed() {
  let wpm = wpmSlider.value;
  printSpd = 12000/wpm;
  wpmDisplay.value = wpm + " wpm";
}


//Open tweet interface in new tab
function tweetQuote() {
  if (overlay.style.display === "block") {
    showCredits();
  }
  let twitterIntent = 'https://twitter.com/intent/tweet?text='+q+" - "+a;
  window.open(twitterIntent, '_blank');
}

//Somewhat hacky method of swapping contents of grid to show credits
function showCredits() {
  if (overlay.style.display !== "block") {
    quoteBox.style.gridTemplateAreas = "'co co co co co co' 'tt tt tt tt tt tt' 'tq tq nq nq cr cr'";
    text.style.display = "none";
    author.style.display = "none";
    overlay.style.display = "block";
    themeToggle.style.display = "block";
  } else {
    quoteBox.style.gridTemplateAreas = "'txt txt txt txt txt txt' 'au au au au au au' 'tq tq nq nq cr cr'";
    text.style.display = "block";
    author.style.display = "block";
    overlay.style.display = "none";
    themeToggle.style.display = "none";
  }
}

//Toggle theme (light or dark)
function toggleTheme() {
  themeElements.forEach((e) => {
    if (e.classList.contains('dark-element')) {
      e.classList.replace('dark-element', 'light-element');
    } else {
      e.classList.replace('light-element', 'dark-element');
    }
  });
}


//Fetch quote on load
getQuote();

//Fetch quote on button
newQuoteBtn.onclick = getQuote;

//Tweet quote
tweetBtn.onclick = tweetQuote;

//Credits overlay
creditsBtn.onclick = showCredits;

//Theme toggle
themeSwitch.onchange = toggleTheme;

//WPM slide
wpmSlider.onchange = changeSpeed;
